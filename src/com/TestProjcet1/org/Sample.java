package com.TestProjcet1.org;
import java.util.*;
public class Sample 
{

	public static void main(String[] args) 
	{
		@SuppressWarnings("resource")
		Scanner scn1=new Scanner(System.in);
		int num;
		{

		System.out.print("Enter an integer number: ");
		num=scn1.nextInt();

		System.out.println("Square of "+ num + " is: "+ Math.pow(num, 2));
		System.out.println("Cube of "+ num + " is: "+ Math.pow(num, 3));
		System.out.println("Square Root of "+ num + " is: "+ Math.sqrt(num));
		}
	}
}
