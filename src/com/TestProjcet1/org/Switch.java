package com.TestProjcet1.org;

public class Switch {

	public static void main(String[] args) {
		int num = 30;
		String name = "your entered number is:";
		switch (num) {
		case 10:
			System.out.println(name + 10);
			break;
		case 20:
			System.out.println(name + 20);
			break;
		case 30:
			System.out.println(name + 30);
			break;
		case 40:
			System.out.println(name + 40);
			break;
		case 50:
			System.out.println(name + 50);
			break;
		default:
			System.out.println("We didn't found your number.");
		}

	}

}
