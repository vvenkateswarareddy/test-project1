package com.TestProjcet1.org;

class Student {

	int rollno;
	String name;
	String college = "SMEC";

	Student(int r, String n) {
		rollno = r;
		name = n;
	}

	void display() {
		
		System.out.println(rollno + " " + name + " " + college);
	}

	public static void main(String[] args) {
		Student S1 = new Student(11, "vvr");
		Student S2 = new Student(12, "vvr2");
		
		S1.display();
		S2.display();
		
	}
		
}
